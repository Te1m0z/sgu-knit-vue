import { createWebHistory, createRouter } from 'vue-router';
// const Home = import('./views/Home.vue');
// const OrgComitet = import('./views/Org-comitet.vue');
// import ProgComitet from './views/Prog-comitet.vue';
import Members from './views/Members.vue';
import Info from './views/Info.vue';
import ClaimsLatex from './views/additional/claims-latex.vue';
import ClaimsWord from './views/additional/claims-word.vue';
import Dates from './views/additional/dates.vue';
import Presenting from './views/additional/presenting.vue';
import Price from './views/additional/price.vue';
import Requirements from './views/additional/requirements.vue';
import NotFound from './views/Not-found.vue';

const routes = [
    {
        path: '/:lang?',
        // alias: '/en',
        component: () => import('./views/Home.vue'),
        meta: {
            title: {
                ru: 'Информационное сообщение | КНиИТ',
                en: 'About conference | "Computer Science and Information Technologies"'
            }
        },
        children: [
            {
                path: 'org-comitet',
                // alias: '/en/org-comitet',
                component: () => import('./views/Org-comitet.vue'),
                meta: {
                    title: {
                        ru: 'Оргкомитет конференции | КНиИТ',
                        en: 'Organizing committee | "Computer Science and Information Technologies"'
                    }
                }
            }
        ]
    },
    // {
    //     path: '/org-comitet',
    //     alias: '/en/org-comitet',
    //     component: () => import('./views/Org-comitet.vue'),
    //     meta: {
    //         title: {
    //             ru: 'Оргкомитет конференции | КНиИТ',
    //             en: 'Organizing committee | "Computer Science and Information Technologies"'
    //         }
    //     }
    // },
    {
        path: '/prog-comitet',
        alias: '/en/prog-comitet',
        component: () => './views/Prog-comitet.vue',
        meta: {
            title: {
                ru: 'Программный комитет | КНиИТ',
                en: 'Program committee | "Computer Science and Information Technologies"'
            }
        }
    },
    {
        path: '/members',
        alias: '/en/members',
        component: Members,
        meta: {
            title: {
                ru: 'Участники | КНиИТ',
                en: 'Participants | "Computer Science and Information Technologies"'
            }
        }
    },
    {
        path: '/info',
        alias: '/en/info',
        component: Info,
        meta: {
            title: {
                ru: 'Информация для участников | КНиИТ',
                en: 'Information | "Computer Science and Information Technologies"'
            }
        }
    },
    {
        path: '/claims-latex',
        alias: '/en/claims-latex',
        component: ClaimsLatex,
        meta: {
            title: {
                ru: 'Требования к оформлению статьи с использованием LaTeX | КНиИТ',
                en: 'LaTeX-templates for preparation of a manuscript | "Computer Science and Information Technologies"'
            }
        }
    },
    {
        path: '/claims-word',
        alias: '/en/claims-word',
        component: ClaimsWord,
        meta: {
            title: {
                ru: 'Требования к оформлению статьи с использованием редактора Microsoft Word | КНиИТ',
                en: 'Microsoft Word template for preparation of a manuscript | "Computer Science and Information Technologies"'
            }
        }
    },
    {
        path: '/dates',
        alias: '/en/dates',
        component: Dates,
        meta: {
            title: {
                ru: 'Важные даты | КНиИТ',
                en: 'Important Dates | "Computer Science and Information Technologies"'
            }
        }
    },
    {
        path: '/presenting',
        alias: '/en/presenting',
        component: Presenting,
        meta: {
            title: {
                ru: 'Представление докладов | КНиИТ',
                en: 'Представление докладов | КНиИТ'
            }
        }
    },
    {
        path: '/price',
        alias: '/en/price',
        component: Price,
        meta: {
            title: {
                ru: 'Стоимость участия | КНиИТ',
                en: 'Стоимость участия | КНиИТ'
            }
        }
    },
    {
        path: '/requirements',
        alias: '/en/requirements',
        component: Requirements,
        meta: {
            title: {
                ru: 'Требования к оформлению текстов докладов | КНиИТ',
                en: 'Guide for Authors | "Computer Science and Information Technologies"'
            }
        }
    },
    {
        path: '/:catchAll(.*)',
        component: NotFound,
        meta: {
            title: {
                ru: 'Страница не найдена!',
                en: 'Page not found!'
            }
        }
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;